package com.lotty.lotty.hermes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), Main2Activity.class));
            }
        },3000);

        /*ImageButton btn_to = (ImageButton) findViewById(R.id.btn_to2);
        btn_to.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(i);
            }
        });*/
    }

   // @Override
   // protected void onStart(){

    //}

  //  @Override
  //  protected void onResume(){

   // }@Override
   // protected void onPause(){

   // }@Override
   // protected void onStop(){

   // }@Override
   // protected void onDestroy(){

   // }@Override
   // protected void onRestart(){

   // }

}
