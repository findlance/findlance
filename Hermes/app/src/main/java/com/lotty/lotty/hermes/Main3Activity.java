package com.lotty.lotty.hermes;

import android.content.Intent;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Intent intent2 = getIntent();
        String Data1 = intent2.getStringExtra("Data01");
        String Data2 = intent2.getStringExtra("Data02");
        String Data3 = intent2.getStringExtra("Data03");
        String Data4 = intent2.getStringExtra("Data04");
        String Data5 = intent2.getStringExtra("Data05");
        String Data6 = intent2.getStringExtra("Data06");
        String Data7 = intent2.getStringExtra("Data07");
        String Data8 = intent2.getStringExtra("Data08");
        String Data9 = intent2.getStringExtra("Data09");
        String Data10 = intent2.getStringExtra("Data10");
        String Data11 = intent2.getStringExtra("Data11");
        final String Data12 = intent2.getStringExtra("Data12");
        final String Data13 = intent2.getStringExtra("Data13");

        final String num_s = Data11;
        final String num_ns = Data11;


        TextView TV_Name = (TextView)findViewById(R.id.tv_name);
        TextView TV_Address = (TextView)findViewById(R.id.tv_address);
        TextView TV_Room = (TextView)findViewById(R.id.tv_room);
        TextView TV_Village = (TextView)findViewById(R.id.tv_Village);
        TextView TV_SideS = (TextView)findViewById(R.id.tv_substreet);
        TextView TV_Street = (TextView)findViewById(R.id.tv_street);
        TextView TV_SubD = (TextView)findViewById(R.id.tv_subdistrict);
        TextView TV_District = (TextView)findViewById(R.id.tv_district);
        TextView TV_Province = (TextView)findViewById(R.id.tv_province);
        TextView TV_Postal = (TextView)findViewById(R.id.tv_postalcode);
        TextView TV_Number = (TextView)findViewById(R.id.tv_number);




        TV_Name.setText(Data1);
        TV_Address.setText(Data2);
        TV_Room.setText(Data3);
        TV_Village.setText(Data4);
        TV_SideS.setText(Data5);
        TV_Street.setText(Data6);
        TV_SubD.setText(Data7);
        TV_District.setText(Data8);
        TV_Province.setText(Data9);
        TV_Postal.setText(Data10);
        TV_Number.setText(Data11);

        Button btn_showMap = (Button)findViewById(R.id.BTN_showMap);
        btn_showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(getBaseContext(),MapsActivity.class);
                intent3.putExtra("Data12",Data12);
                intent3.putExtra("Data13",Data13);
                startActivity(intent3);
            }
        });

        Button btn_sms = (Button)findViewById(R.id.BTN_sms1);
        btn_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmsManager smsMan = SmsManager.getDefault();
                String sendto = num_s;
                String message = "จดหมาย/พัสดุกำลังจะไป ณ ที่อยู่ของท่าน โดย บ.ไปรษณีย์ไทย";
                smsMan.sendTextMessage(sendto,null,message,null,null);
            }
        });

        Button btn_sms2 = (Button)findViewById(R.id.BTN_sms2);
        btn_sms2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmsManager smsMan1 = SmsManager.getDefault();
                String sendto1 = num_ns;
                String message1 = "กรุณามารับพัสดุของท่านได้ ณ ไปรษณีย์ใกล้บ้านคุณ โดย บ.ไปรษณีย์ไทย";
                smsMan1.sendTextMessage(sendto1,null,message1,null,null);
            }
        });

        }
    }

