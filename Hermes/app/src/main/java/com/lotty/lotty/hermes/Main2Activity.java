package com.lotty.lotty.hermes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    public static final int REQUEST_QR_SCAN = 4;
    TextView textContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //textContent = (TextView)findViewById(R.id.textContent);

        ImageButton buttonIntent = (ImageButton)findViewById(R.id.btn_QrCode2);
        buttonIntent.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent =
                        new Intent("com.google.zxing.client.android.SCAN");
                startActivityForResult(Intent.createChooser(intent
                        , "Scan with"), REQUEST_QR_SCAN);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode
            , Intent intent) {
        String contents = "0" ;
        if (requestCode == REQUEST_QR_SCAN && resultCode == RESULT_OK) {
            contents = intent.getStringExtra("SCAN_RESULT");
           /* textContent.setText(contents);*/
        }

        String[] name1 = new String[13];
        name1 = contents.split(",");

        Intent intent2 = new Intent(getBaseContext(),Main3Activity.class);
        intent2.putExtra("Data01",name1[0]);
        intent2.putExtra("Data02",name1[1]);
        intent2.putExtra("Data03",name1[2]);
        intent2.putExtra("Data04",name1[3]);
        intent2.putExtra("Data05",name1[4]);
        intent2.putExtra("Data06",name1[5]);
        intent2.putExtra("Data07",name1[6]);
        intent2.putExtra("Data08",name1[7]);
        intent2.putExtra("Data09",name1[8]);
        intent2.putExtra("Data10",name1[9]);
        intent2.putExtra("Data11",name1[10]);
        intent2.putExtra("Data12",name1[11]);
        intent2.putExtra("Data13",name1[12]);
        startActivity(intent2);
        }


        /*String[] name = "Hello,World".split(",");

        for (int i = 0; i < name.length; i++) {
            System.out.println(name[i]);*/
        //}

    }



    //@Override
   // protected void onStart(){

   // }

    //@Override
    //protected void onResume()
   // {

   // }
    //@Override
   // protected void onPause()
    //{

   // }
   // @Override
   // protected void onStop()
    //{

    //}
    //@Override
   // protected void onDestroy()
    //{

    //}
    //@Override
    //protected void onRestart()
    //{

    //}

