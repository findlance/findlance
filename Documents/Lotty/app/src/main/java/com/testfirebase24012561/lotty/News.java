package com.testfirebase24012561.lotty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class News extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        final ListView Mylistview = (ListView) findViewById(R.id.mylistview);

        String[] values = new String[] {"News1","News2","News3","News4","News5","News6"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,values);
        Mylistview.setAdapter(adapter);
        Mylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int itemPosition = i;
                String itemValue = (String) Mylistview.getItemAtPosition(itemPosition);
                Toast.makeText(News.this, "Position : " + itemPosition + " Listitem : " + itemValue, Toast.LENGTH_LONG).show();
            }
        });
    }
}
