package com.testfirebase24012561.lotty;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //ActionBar actionBar = getActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ImageButton btn_to = (ImageButton) findViewById(R.id.img_balance);
        btn_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Balance.class);
                startActivity(i);
            }
        });

        ImageButton btn_to2 = (ImageButton) findViewById(R.id.img_news);
        btn_to2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(getApplicationContext(), News.class);
                startActivity(i2);
            }
        });

        ImageButton btn_to3 = (ImageButton) findViewById(R.id.img_checknum);
        btn_to3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i3 = new Intent(getApplicationContext(), CheckReward.class);
                startActivity(i3);
            }
        });

        ImageButton btn_to4 = (ImageButton) findViewById(R.id.img_account);
        btn_to4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i4 = new Intent(getApplicationContext(), PaymentRate.class);
                startActivity(i4);
            }
        });

        ImageButton btn_to5 = (ImageButton) findViewById(R.id.img_benefit);
        btn_to5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i5 = new Intent(getApplicationContext(), VIPregister.class);
                startActivity(i5);
            }
        });

        ImageButton btn_to6 = (ImageButton) findViewById(R.id.img_contactus);
        btn_to6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i6 = new Intent(getApplicationContext(), ContactUs.class);
                startActivity(i6);
            }
        });

        ImageButton btn_to7 = (ImageButton) findViewById(R.id.img_play);
        btn_to7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i7 = new Intent(getApplicationContext(), Play.class);
                startActivity(i7);
            }
        });

    }



}
