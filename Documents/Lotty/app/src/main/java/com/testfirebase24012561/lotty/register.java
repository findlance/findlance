package com.testfirebase24012561.lotty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class register extends AppCompatActivity {

    private Button mrgbutton;

    private EditText mrgusername;
    private EditText mrgpassword;
    private EditText mrgcfpassword;
    private EditText mrgemail;

    private FirebaseAuth mAuth;
    private DatabaseReference mdatabase = FirebaseDatabase.getInstance().getReference().child("Users");

    private ProgressDialog mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        mProgress = new ProgressDialog(this);

        mrgusername = (EditText) findViewById(R.id.rgusername);
        mrgpassword = (EditText) findViewById(R.id.rgpassword);
        mrgcfpassword = (EditText) findViewById(R.id.rgcfpassword);
        mrgemail = (EditText) findViewById(R.id.rgemail);

        mrgbutton = (Button) findViewById(R.id.rgbutton);

        mrgbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegister();

            }
        });
    }

    private void startRegister() {

        final String mname = mrgusername.getText().toString().trim();
        String email = mrgemail.getText().toString().trim();
        String password = mrgpassword.getText().toString().trim();
        String cfpassword = mrgcfpassword.getText().toString().trim();

        if(!TextUtils.isEmpty(mname) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(cfpassword)){

            mProgress.setMessage("Signing up ...");
            mProgress.show();

            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){

                        String user_id =mAuth.getCurrentUser().getUid();

                        DatabaseReference current_user_db = mdatabase.child(user_id);

                        current_user_db.child("name").setValue(mname);
                        current_user_db.child("image").setValue("default");
                        current_user_db.child("MoneyTotal").setValue("0");

                        mProgress.dismiss();

                        Intent mainIntent = new Intent(register.this,Login02.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainIntent);

                    }
                }
            });

        }

    }

}