package com.testfirebase24012561.lotty;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.IDNA;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.net.IDN;

public class Play extends AppCompatActivity {

    private Button btntorule;
    private NotificationManager mNotif;
    private final int ID = 999;
    EditText et1,et2,et3;
    public String s1,s2,s3;
    public int int1,int2,int3;
    public EditText editText01;
    public EditText editText02;
    public EditText editText03;

    //NotificationCompat.Builder notification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        btntorule=(Button) findViewById(R.id.btn_playrule);
        btntorule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent irule = new Intent(getApplicationContext(), PlayRule.class);
                startActivity(irule);
            }
        });


    Button butnoti = (Button)findViewById(R.id.btn_Confirm);
    butnoti.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            editText01 = (EditText) findViewById(R.id.editText1);
             int1 = Integer.parseInt(editText01.getText().toString());
            editText02 = (EditText) findViewById(R.id.editText2);
             int2 = Integer.parseInt(editText02.getText().toString());
            editText03 = (EditText) findViewById(R.id.editText3);
             int3 = Integer.parseInt(editText03.getText().toString());
           /* s1 = et1.toString();
            int int1 = Integer.parseInt(s1);
            s2 = et2.toString();
            int int2 = Integer.parseInt(s2);
            s3 = et3.toString();
            int int3 = Integer.parseInt(s3);*/
            if ((int1 >= 10 && int1 <= 99) || (int2 >= 100 && int2 <= 999) || (int3 >= 100000 && int3 <= 999999)) {
                showNotification();
                Intent iplay = new Intent(getApplicationContext(), Main.class);
                startActivity(iplay);
            }

            /*else
                createMessage();
                Intent iplay2 = new Intent(getApplicationContext(), Play.class);
            startActivity(iplay2);
            }*/
            // }
            }
       });}

    private void  showNotification() {

        Notification.Builder builder= new Notification.Builder(getBaseContext());
        builder.setSmallIcon(R.drawable.welcome2);
        builder.setContentTitle("ทำการเลือกตัวเลขเรียบร้อย");
        builder.setContentText("ท่านได้ทำการเข้าเล่นเรียบร้อย โปรดตรวจสอบยอดเงินในบัญชี");

        Notification notification = builder.build();
        mNotif = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        mNotif.notify(ID,notification);
    }

    private void createMessage() {

        new AlertDialog.Builder(Play.this).setIcon(R.drawable.welcome2).setTitle("ผิดพลาด").setMessage("กรุณาใส่เลขให้ตรงกับเงื่อนไข").setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "กำลังดำเนินการ", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
